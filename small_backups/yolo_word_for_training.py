import nltk
from nltk.corpus import wordnet
import os
import time
import json
import multiprocessing as mp
import numpy as np
import torch


token_file = open(r"F:\Bachelor\gpt-2-pytorch-me\vocab.json", "r")
tokens = json.load(token_file)
token_keys = list(tokens.keys())

yolo_vocab_file = open("F:\Bachelor\OR\darknet\data\9k.names")
yolo_vocabs = yolo_vocab_file.read()
yolo_vocabs = yolo_vocabs.replace(' ', '_')
yolo_vocabs = yolo_vocabs.split("\n")
ignore_symbols = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "@", "!", "§", "$", "%", "&", "(", ")", "=", "?",
                  "+", "-", ",", ".", ":", ";","*", "#", "'", '"', "~", "\\", "/", "€", "<", ">", "|", "^", "°",
                  "<|endoftext|>", 'âĢ¦."', "(/", "×©"]


def get_sequence_of_words(value):
    word = token_keys[value]
    # if word[0] == "\u0120":
    #     word = word[1:]
    return word


def find_syn(input_string):
    """
    finds synonyms and antonyms for a given string
    @param input_string: string to find synonyms for
    @return: returns the synonyms and antonyms
    """
    synonyms = []
    string = wordnet.synset(input_string+".n.01")
    for syn in wordnet.synsets(input_string):
        for l in syn.lemmas():
            sim = syn.wup_similarity(string) if syn.wup_similarity(string) is not None else 0
            if sim > 0.25:
                synonyms.append(l.name())
    return set(synonyms)


def find_yolo_words(input):
    if input[0] == "\u0120":
        word = input[1:]
    else:
        word = input
    try:
        syn = find_syn(word)
    except nltk.corpus.reader.wordnet.WordNetError:
        syn = word
    yolo_words = []
    used_seq_word = ""
    for i in syn:
        if i in yolo_vocabs and i != []:
            yolo_words.append(i)
            yolo_words.append("\u0120" + i)
            used_seq_word = input
    return yolo_words, used_seq_word


def remove_unwanted_symbols(sequence):
    new_seq = []
    for i in sequence:
        if not (i in ignore_symbols or type(i) == 'unicode' or len(i) == 1):
            new_seq.append(i)
    return new_seq


def words_tokenizing(words_from_seq):
    used_words = np.array([], dtype=np.int)
    for w in words_from_seq:
        try:
            tok = tokens[w]
        except KeyError:
            tok = -1
        used_words = np.append(used_words, tok)
    return used_words


def yolo_encoding(sequence):
    pool = mp.Pool(mp.cpu_count())
    # sequence = np.array(sequence.detach(), dtype=np.int)
    # sequence_mod = sequence[sequence <= 50256]
    seq_words = pool.map(get_sequence_of_words, [seq for seq in sequence])
    seq_words = remove_unwanted_symbols(seq_words)
    # yolo_words, words_from_seq = zip(*pool.map(find_yolo_words, [word for word in seq_words]))
    yolo_words, words_from_seq, seq_word_look_up = np.array([]), np.array([]), {}
    for word in seq_words:
        y, w = find_yolo_words(word)
        if y:
            y = words_tokenizing(y)
            y = y[y != -1]
            yolo_words = np.append(yolo_words, y)
            w = word_tokenizing(w)
            if w not in words_from_seq:
                words_from_seq = np.append(words_from_seq, w)
            for i in y:
                if i not in seq_word_look_up.keys():
                    seq_word_look_up[i] = w
            # if w in yolo_look_up.keys():
            #     yolo_look_up[w] = np.append(yolo_look_up[w], y)
            # else:
            #     yolo_look_up[w] = y
    # yolo_words_arr = np.array([], dtype=np.str)
    # for i in yolo_words:
    #     if i:
    #         yolo_words_arr = np.append(yolo_words_arr, i)
    # used_words = np.unique(used_words_tokens(words_from_seq))  # is also ordered, might be something to look into later
    # results = np.array(pool.map(yolo_words_encoding, [word for word in yolo_words_arr]))
    # pool.close()
    # used_words = used_words[used_words != -1]
    return torch.as_tensor(yolo_words, dtype=torch.int64), seq_word_look_up, words_from_seq


def word_tokenizing(yolo_word):
    try:
        return tokens[yolo_word]
    except KeyError:
        return -1
