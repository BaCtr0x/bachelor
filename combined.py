import numpy as np
import cv2
# from models.yolo9000.darknet.darknet import load_network, detect_image, load_meta


def run_YOLO_on_image(imagedirect):
    INPUT_FILE = imagedirect
    root_dir = "/Bachelor/models/yolo9000/"
    root_dir_darkent = root_dir + "darknet/"
    OUTPUT_FILE = 'predeicted.jpg'
    LABELS_FILES = root_dir_darkent + 'data/9k.names'
    CONFIG_FILE = root_dir_darkent + 'cfg/yolov2.cfg'
    WEIGHTS_FILE = root_dir + 'yolo-9000-weights/yolov2.weights'
    COCO9K_MAP = root_dir_darkent + 'data/coco9k.map'
    CONFIDENCE_THRESHOLD = 0.5

    LABELS = open(LABELS_FILES).read().strip().split('\n')
    MAPPING = open(COCO9K_MAP).read().strip().split('\n')

    np.random.seed(4)
    COLORS = np.random.randint(0, 255, size=(len(LABELS), 3), dtype="uint8")

    net = cv2.dnn.readNet(CONFIG_FILE, WEIGHTS_FILE)

    # net = load_network(CONFIG_FILE, WEIGHTS_FILE, 0)
    # meta = load_meta(COCO9K_MAP)
    # results = detect_image(net, meta, "data/dog.jpg")
    # print(results)
    # exit()

    image = cv2.imread(INPUT_FILE)
    (H, W) = image.shape[:2]

    # determine only the *output* layer names that we need from YOLO
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)
    layerOutput = net.forward(ln)

    # initialize our list of detected bounding boxes, confidences, and class IDs, respectively
    boxes = []
    confidences = []
    classIDs = []

    # loop over each of the layer outputs
    for output in layerOutput:
        # loop over each of the detections
        for detection in output:
            # extract the class ID and confidence (i.e., probability) of the current object detection
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]

            # filter out weak predictions by ensuring the detected probability is greater than the minimum probability
            if confidence > CONFIDENCE_THRESHOLD:
                # scale the bounding box coordinates back relative to the size of the image, keeping in mind that YOLO
                # actually returns the center (x, y)-coordinates of the bounding box followed by the boxes width and height
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")

                # use the center (x, y)-coordinates to derive the top and left corner of the bounding box
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                # update our list of bounding box coordinates, confidences, and classIDs
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                classIDs.append(classID)

    # apply non-maxima supression to supress weak, overlapping bounding boxes
    idxs = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE_THRESHOLD, CONFIDENCE_THRESHOLD)

    predicted_objects = []

    for i in classIDs:
        mapped = int(MAPPING[i])
        predicted_objects.append(LABELS[mapped])
    predicted_objects = np.array(predicted_objects)
    unique_predicted_objects = np.unique(predicted_objects)
    return unique_predicted_objects


if __name__ == "__main__":
    image_path = "data/kitchen.jpg"
    run_YOLO_on_image(image_path)
