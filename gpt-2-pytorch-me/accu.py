import argparse
import json
import os
import numpy as np
import time
import torch


class AccumulatingOptimizer(object):
    def __init__(self, opt, var_list):
        self.opt = opt
        self.var_list = var_list
        self.accum_vars = {tv: np.zeros(np.shape(tv)) for tv in var_list}
        self.total_loss = np.zeros([], dtype=np.float)
        self.count_loss = np.zeros([], dtype=np.float)

    def reset(self):
        updates = np.array([np.zeros(tv) for tv in self.accum_vars])
        np.append(updates, self.total_loss)
        np.append(updates, self.count_loss)
        return torch.tensor(updates, dtype=torch.float32)

    def compute_gradients(self, loss):
        grads = self.opt.compute_gradients(loss, self.var_list)
        updates = [np.add(self.accum_vars[v], g) for (g, v) in grads]
        updates = np.append(np.add(self.total_loss, loss))
        updates = np.append(np.add(self.count_loss, 1.0))
        return torch.tensor(updates, dtype=torch.float32)

    def apply_gradients(self):
        # seems wrong :D
        grads = [(g, v) for (v, g) in self.accum_vars]
        app_gradients = self.opt.apply_gradients(grads)
        return self.total_loss / self.count_loss
