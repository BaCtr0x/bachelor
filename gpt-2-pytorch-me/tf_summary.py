from tensorflow.python.summary.summary_iterator import summary_iterator
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

sns.set_theme(style="darkgrid")


def get_tfboard_information(path):
    val_accuracy = np.array([])
    loss = np.array([])
    val_nll = np.array([])
    val_average_nll = np.array([])
    val_average_accuracy = np.array([])
    val_average_ppl = np.array([])
    lr_group = np.array([])
    for summary in summary_iterator(path):
        for v in summary.summary.value:
            v_value = v.simple_value
            v_tag = v.tag
            if v_tag == "validation/accuracy":
                val_accuracy = np.append(val_accuracy, v_value)
            elif v_tag == "training/loss":
                loss = np.append(loss, v_value)
            elif v_tag == "validation/nll":
                val_nll = np.append(val_nll, v_value)
            elif v_tag == "validation/average_nll":
                val_average_nll = np.append(val_average_nll, v_value)
            elif v_tag == "validation/average_accuracy":
                val_average_accuracy = np.append(val_average_accuracy, v_value)
            elif v_tag == "validation/average_ppl":
                val_average_ppl = np.append(val_average_ppl, v_value)
            else:
                lr_group = np.append(lr_group, v_value)

    steps = np.arange(loss.size)
    epochs = np.arange(val_accuracy.size)
    data_frame_epochs = pd.DataFrame({"validation/accuracy": val_accuracy, "validation/nll": val_nll,
                               "validation/average_nll": val_average_nll, "validation/average_accuracy": val_average_accuracy,
                               "validation/average_ppl": val_average_ppl, "epochs": epochs})
    data_frame_steps = pd.DataFrame({"training/loss": loss, "learning_rate": lr_group, "steps": steps})

    return data_frame_epochs, data_frame_steps


def plot_loss_learning_rate(data):
    fig, axis = plt.subplots(ncols=1, nrows=2)
    sns.lineplot(x="steps", y="training/loss", data=data, ax=axis[0])
    sns.lineplot(x="steps", y="learning_rate", data=data, ax=axis[1])
    plt.subplots_adjust(wspace=0.5, hspace=0.5)
    return fig


def plot_all(data_frame_epochs, data_frame_steps):
    fig, axis = plt.subplots(ncols=2, nrows=4)
    fig.set_figheight(8)
    fig.set_figwidth(16)
    sns.lineplot(x="steps", y="training/loss", data=data_frame_steps, ax=axis[0][0])
    sns.lineplot(x="steps", y="learning_rate", data=data_frame_steps, ax=axis[0][1])
    sns.lineplot(x="epochs", y="validation/accuracy", data=data_frame_epochs, ax=axis[1][0])
    sns.lineplot(x="epochs", y="validation/average_accuracy", data=data_frame_epochs, ax=axis[1][1])
    sns.lineplot(x="epochs", y="validation/nll", data=data_frame_epochs, ax=axis[2][0])
    sns.lineplot(x="epochs", y="validation/average_nll", data=data_frame_epochs, ax=axis[2][1])
    sns.lineplot(x="epochs", y="validation/average_ppl", data=data_frame_epochs, ax=axis[3][0])
    plt.subplots_adjust(wspace=1, hspace=1)
    return fig


def save_plot(fig, data_path, name_addon):
    model_name = data_path.split("/events")[0].split("/")[-1]
    path = "plots/{}".format(model_name)
    file_path = "{}/{}_{}".format(path, model_name, name_addon)
    if not os.path.isdir(path):
        os.mkdir(path)
    elif os.path.isfile(file_path + ".png"):
        print("Do you want to override the current file: {}? yes/no".format(file_path + ".png"))
        answer = str(input())
        if answer == "yes":
            os.remove(file_path + ".png")
            fig.savefig(file_path)
    else:
        fig.savefig(file_path)


if __name__ == "__main__":
    path_250 = "external_runs/Mar03_10-47-29_612bd55040a3_gpt2/events.out.tfevents.1614768451.612bd55040a3.345.0"
    path_1000 = "external_runs/Mar03_11-16-59_612bd55040a3_gpt2/events.out.tfevents.1614770221.612bd55040a3.597.0"
    data_epochs, data_steps = get_tfboard_information(path_1000)
    figure = plot_all(data_epochs, data_steps)
    figure_loss_lr = plot_loss_learning_rate(data_steps)
    save_plot(figure, path_250, "all")
    save_plot(figure_loss_lr, path_250, "loss_lr")
    plt.show()
