import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from yolo_word_for_training import find_syn, get_sequence_of_words
from huggingface_training import get_data_loaders, get_dataset
from transformers import GPT2Tokenizer, cached_path
from tqdm import tqdm
import nltk

import os
import torch
import logging
import json

logger = logging.getLogger(__file__)

logger = logging.getLogger(__name__)

sns.set_theme(style="darkgrid")
checkpoint = "external_runs/Mar03_11-16-59_612bd55040a3_gpt2"
tokenizer = GPT2Tokenizer.from_pretrained(checkpoint)


def saving_huggingface_data(dataset_path, PERSONACHAT_URL, dataset_cache):
    dataset_path = dataset_path or PERSONACHAT_URL
    dataset_cache = dataset_cache + '_' + type(tokenizer).__name__  # To avoid using GPT cache for GPT-2 and vice-versa
    if dataset_cache and os.path.isfile(dataset_cache):
        logger.info("Load tokenized dataset from cache at %s", dataset_cache)
        dataset = torch.load(dataset_cache)
    else:
        logger.info("Download dataset from %s", dataset_path)
        personachat_file = cached_path(dataset_path)
        with open(personachat_file, "r", encoding="utf-8") as f:
            dataset = json.loads(f.read())
    data = np.array([])
    for i in tqdm(dataset['train']):
        for ut in i['utterances']:
            for can in ut['candidates']:
                data = np.append(data, can)

    file = open("dataset_file", "wb")
    np.save(file, data)
    file.close()

    # file = open("dataset_file", "rb")
    # a = np.load(file)
    # print(a)


def decode_dataset(dataset_path):
    save_path = "decoded_dataset"
    file = open(dataset_path, "rb")
    data = np.load(file)
    dataset_words = np.array([], dtype=np.str)
    for token in tqdm(data):
        word = get_sequence_of_words(token)
        dataset_words = np.append(dataset_words, word)
    file.close()
    word_file = open(save_path, "wb")
    np.save(word_file, dataset_words)
    word_file.close()


def synonym_histogram(sequence):
    seq_words = [get_sequence_of_words(token) for token in sequence]
    syn = [find_syn(word) for word in seq_words]
    data_frame = pd.DataFrame({"synonyms": syn, "seq_words": seq_words})

    sns.histplot(data_frame, x="seq_words", y="synonyms")
    plt.show()


def json_to_array(vocab_path):
    vocab_file = open(vocab_path, "r")
    vocab = json.load(vocab_file)
    keys = list(vocab.keys())
    words = np.array([], dtype=np.str)
    for key in tqdm(keys):
        word = get_sequence_of_words(vocab[key])
        words = np.append(words, word)
    return words


def create_syn_dict(words):
    syn_dict = dict(zip(words, ""))
    syn_arr = np.array([])
    for w in tqdm(words):
        try:
            syn = find_syn(w)
            syn_dict[w] = syn
            syn_arr = np.append(syn_arr, len(syn))
        except nltk.corpus.reader.wordnet.WordNetError:
            pass
    return syn_dict, syn_arr


if __name__ == "__main__":
    dataset_path = "dataset_file"
    cache_path = "./dataset_cache"
    vocab_path = "vocab.json"
    persona_chat_url = "https://s3.amazonaws.com/datasets.huggingface.co/personachat/personachat_self_original.json"
    # saving_huggingface_data(dataset_path, persona_chat_url, cache_path)
    # decode_dataset(dataset_path)
    words = json_to_array(vocab_path)
    syn_dic, syn_arr = create_syn_dict(words)
    data_frame = pd.DataFrame({"words": list(syn_dic.keys()), "synonym_count": syn_arr})
    print(data_frame)
    fig, axis = plt.subplots()
    sns.displot(data_frame, x="synonym_count")
    fig.savefig("vocab_histogram")
    plt.show()
