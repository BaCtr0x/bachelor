To get the dataset used to train the model, just run the download_dataset_gpt2.py file.
It is the implementation from Huggingface.
This will download a webtext datasubset from huggingface and store it in a folder called data.
The standard huggingface dataset downloaded for the model if no specific dataset is given a webtext subset, therefore
the download is mainly usefull to get a look into the dataset.


Thanks Huggingface for providing such an easy way to get nice to use datasets.
