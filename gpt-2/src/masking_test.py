import numpy as np
import sys
import tensorflow as tf


def unmasking_future(nd, ns, future_span):
    with tf.Session() as sess:
        a = np.arange(nd)[:, None]
        b = np.arange(ns)
        arr = np.array([], dtype=np.int)
        front_padding = 1
        end_padding = b.size - future_span - 1
        for i in range(0, a.size):
            if b.size - i > future_span:
                x = np.random.randint(0, 2, future_span)
                x = np.pad(x, pad_width=(front_padding, end_padding))
            elif b.size - i > future_span - 3:
                new_future_span = future_span - 3
                if front_padding + end_padding + new_future_span != nd:
                    end_padding = nd - (front_padding + end_padding + new_future_span)
                x = np.random.randint(0, 2, new_future_span)
                x = np.pad(x, pad_width=(front_padding, end_padding))
            else:
                x = np.zeros(ns, dtype=np.int)
            arr = np.append(arr, x, axis=0)
            front_padding += 1
            if end_padding != 0:
                end_padding -= 1
        arr = arr.reshape((nd, ns))
        c = a > b
        w = np.add(c, arr)
        return w


def unmasking_future_tf(nd, ns, future_span):
    """
    This function is unmasking a random amount of future words in a given span, moving them with the predicted words
    @param nd: the amounts of rows in the mask
    @param ns: the amount of columns in the mask
    @param future_span: the span in which random words are supposed to be picked for unmasking
    @return: an array that will be added to the original mask to unmask these words (adding 1's to masked positions {0's})
    """
    with tf.Session() as sess:
        a = tf.range(nd)[:, None]
        b = tf.range(ns)
        a_size = tf.size(a)
        b_size = sess.run(tf.size(b))
        arr = tf.constant([], dtype=tf.int64)
        front_padding = 1
        end_padding = b_size - future_span - 1
        for i in range(0, 16):
            if b_size - i > future_span:
                x = np.random.randint(0, 2, future_span)
                x = np.pad(x, pad_width=(front_padding, end_padding))
            elif b_size - i > future_span - 3:
                new_future_span = future_span - 3
                if front_padding + end_padding + new_future_span != nd:
                    end_padding = b_size - (front_padding + end_padding + new_future_span)
                x = np.random.randint(0, 2, new_future_span)
                x = np.pad(x, pad_width=(front_padding, end_padding))
            else:
                x = np.zeros(a_size, dtype=np.int)
            arr = tf.stack(arr, x, axis=0)
            front_padding += 1
            if end_padding != 0:
                end_padding -= 1
        arr = arr.reshape((a_size, b_size))
        c = a > b
        c = tf.cast(c, tf.int64)
        d = tf.add(c, arr)
        np.set_printoptions(threshold=sys.maxsize)
        print(sess.run(d))


n = 16
m = 16
f = 5
unmasking_future(n, m, f)
