import torch
import numpy as np

ma = torch.randn(9,9)
h, w = ma.size()
indices = torch.triu_indices(h, w, offset=0)
ind = indices[1]
print(ind)
value_tens = torch.zeros(0, len(indices[1]))
ind = torch.where(ind > 4, ind, 0)
print(ind)