from transformers import BertTokenizer
from transformers import XLNetTokenizer
from transformers import OpenAIGPTTokenizer

from tokenizers import Tokenizer
from tokenizers.models import BPE
from tokenizers.trainers import BpeTrainer
from tokenizers.pre_tokenizers import Whitespace



"""Bert Tokennizer"""


def tokenize_with_Bert(input_sentence):
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    tokenized = tokenizer.tokenize(input_sentence)
    print(tokenized)


# tokenize_with_Bert("I have a new GPU!")


def tokenize_with_XLNet(input_sentence):
    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
    tokenized = tokenizer.tokenize(input_sentence)
    print(tokenized)


# tokenize_with_XLNet("Don't you love 🤗 Transformers? We sure do.")


def tokenize_with_GPT(input_sentence):
    tokenizer = OpenAIGPTTokenizer.from_pretrained('openai-gpt')
    tokenized = tokenizer.tokenize(input_sentence)
    print(tokenized)


tokenize_with_GPT("Don't you love 🤗 Transformers? We sure do.")


def tokenize_with_BPEmodel(inputsentence=None):
    tokenizer = Tokenizer(BPE())
    trainer = BpeTrainer(special_tokens=["[UNK]", "[CLS]", "[SEP]", "[PAD]", "[MASK]"])
    tokenizer.pre_tokenizer = Whitespace()

    # files = [f"data/wikitext-103-raw/wiki.{split}.raw" for split in ["test", "train", "valid"]]
    # tokenizer.train(trainer, files)

    # tokenizer.save("data/tokenizer-wiki.json")

    tokenizer = Tokenizer.from_file("data/tokenizer-wiki.json")

    output = tokenizer.encode("Hello, y'all! How are you 😁 ?")

    print(output.tokens)
    print(output.ids)
    print(output.offsets[9])
    tokenizer.token_to_id("[SEP]")


# tokenize_with_BPEmodel()
