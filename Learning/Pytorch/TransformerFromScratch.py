import json
import os

import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
import torch.distributions as dist
from torchvision import models
from torchsummary import summary

import time
import tqdm
import random

import psutil

# imports the encoder from gpt-2
import encoder  # imports the encoder from gpt-2
import sample  # imports the sample function from gpt-2 as pytorch version
from load_dataset import load_dataset, Sampler
from MyTransformer import Transformer, default_hparams


from torch.utils.tensorboard import SummaryWriter
import argparse


# creates a parser to specify the training information
parser = argparse.ArgumentParser(
    description='Train generate-transformer on a custom dataset.',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# adding the different specification options and defining default values
parser.add_argument('--dataset', metavar='PATH', type=str, default="data/test_data.txt", required=True, help='Input file, directory, or glob pattern (utf-8 text, or preencoded .npz files).')
parser.add_argument('--combine', metavar='CHARS', type=int, default=50000, help='Concatenate input files with <|endoftext|> separator into chunks of this minimum size')
parser.add_argument('--encoder_path', default="encoder.json", help="This is the path to the encoder.json file to definde the encoder.")
parser.add_argument('--encoding', type=str, default='utf-8', help='Set the encoding for reading and writing files.')
parser.add_argument('--vocab_path', default="vocab.bpe", help="This is the path to the vocab.bpe file to definde the vocabulary.")
parser.add_argument('--batch_size', metavar='SIZE', type=int, default=1, help='Batch size')
parser.add_argument('--learning_rate', metavar='LR', type=float, default=0.00002, help='Learning rate for Adam')
parser.add_argument('--lr-warmup', dest="lr_warmup",  default=5000, type=int, help="Learning rate warmup.")
parser.add_argument('--optimizer', type=str, default='adam', help='Optimizer. <adam|sgd>.')
parser.add_argument('--noise', type=float, default=0.0, help='Add noise to input training data to regularize against typos.')
parser.add_argument('--hparams_path', type=str, default="", help="The path to the hyper parameters directory.")

parser.add_argument('--sample_every', metavar='N', type=int, default=100, help='Generate samples every N steps')
parser.add_argument('--sample_length', metavar='TOKENS', type=int, default=1023, help='Sample this many tokens')

parser.add_argument('--val_dataset', metavar='PATH', type=str, default=None, help='Dataset for validation loss, defaults to --dataset.')
parser.add_argument('--val_batch_size', metavar='SIZE', type=int, default=2, help='Batch size for validation.')
parser.add_argument('--val_batch_count', metavar='N', type=int, default=40, help='Number of batches for validation.')
parser.add_argument('--val_every', metavar='STEPS', type=int, default=0, help='Calculate validation loss every STEPS steps.')
parser.add_argument('--random-seed', dest="seed", default=1, type=int, help="RNG seed. Negative for random")
parser.add_argument('--tb_dir', dest="tb_dir", default='./runs', help="Tensorboard logging directory")

# path variable to save the later generated sample and a checkpoint
SAMPLE_DIR = 'samples'
CHECKPOINT_DIR = 'checkpoint'


def maketree(path):
    """
    It creates a directory to save the models checkpoint other information to
    :param path: string for the directory
    """
    try:
        os.makedirs(path)
    except:
        pass


def randomize(context, hparams, p):
    """
    Randomizes the context depending on the noise and mask that are set
    :param context: the context that will be randomized
    :param hparams: the hyper parameters of the model, containing the number of tokens that is needed
    :param p: max value for the uniform distribution
    :return: returns the created context
    """
    if p > 0:
        # the shape of the context
        shape = context.shape()
        # create the mask as a random uniform distribution between 0 and p with a shape of the context
        mask = torch.FloatTensor(shape[0], shape[1]).uniform_(0, p)
        # create the mask as a random uniform distribution between 0 and #vocabularies with a shape of the context
        noise = torch.FloatTensor(shape[0], shape[1]).uniform_(0, hparams["n_vocab"])
        # mask = tf.random.uniform(shape=tf.shape(context)) < p
        # noise = tf.random.uniform(shape=tf.shape(context), minval=0, maxval=hparams.n_vocab, dtype=tf.int32)
        return torch.where(mask, noise, context)
    else:
        return context


def dev(tensor=None):
    """
    Returns a device string either for the best available device,
    or for the device corresponding to the argument
    :param tensor:
    :return:
    """
    if tensor is None:
        return 'cuda' if torch.cuda.is_available() else 'cpu'
    return 'cuda' if tensor.is_cuda else 'cpu'


def sample(lnprobs, temperature=1.0):
    """
    Sample an element from a categorical distribution
    :param lnprobs: Outcome log-probabilities
    :param temperature: Sampling temperature. 1.0 follows the given distribution, 0.0 returns the maximum probable element
    :return: the index of the sample element
    """
    # return the maximum probable element if wanted
    if temperature == 0.0:
        return lnprobs.argmax()

    # calculates the probability in context of the temperature
    p = F.softmax(lnprobs / temperature, dim=0)
    # gets the categorical distribution
    cd = dist.Categorical(p)

    return cd.sample()


def training(args):
    """
    This is the function to train the model
    :param arg: are the argument for the models training
    :return: None
    """

    # check if seed is set, else set a random seed
    if args.seed < 0:
        seed = random.randint(0, 1000000)
        print('random seed: {} has been set.'.format(seed))
    else:
        torch.manual_seed(args.seed)

    # Tensorboard logging
    tbw = SummaryWriter(log_dir=args.tb_dir)

    # loads the hparams from a json, if specified
    if args.hparams_path != "":
        with open(os.path.join(args.hparams_path, 'hparams.json')) as f:
            hparams = json.load(f)
    else:
        hparams = default_hparams()

    # define the encoder
    enc = encoder.get_encoder(args.encoder_path, args.vocab_path)

    # checks if the sample lenght is within the window size
    if args.sample_length > hparams["n_ctx"]:
        raise ValueError(
            "Can't get samples longer than window size: %s" % hparams["n_ctx"])

    print(hparams)

    # storing the currently used amount of memory
    # memory_usage = psutil.virtual_memory()._asdict()

    # prints the currently used amount of memory in percent
    # print(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total)
    # exit()
    # create the model and move it to the GPU if possible
    model = Transformer(hparams=hparams)
    if torch.cuda.is_available():
        model.cuda()

    print("Model has been created.")
    exit()

    # set the optimizer depending on the input
    if args.optimizer == "adam":
        opt = torch.optim.Adam(lr=args.learning_rate, params=model.parameters())
    elif args.optimizer == "sgd":
        opt = torch.optim.SGD(lr=args.learning_rate, params=model.parameters())
    else:
        exit('Bad optimizer', args.optimizer)

    # linear learning rate warmup
    sch = torch.optim.lr_scheduler.LambdaLR(opt, lambda i: min(i / (args.lr_warmup / args.batch_size), 1.0))

    # training the model
    # - note: we don't loop over the data, instead we sample a batch of random subsequences each time.
    for i in tqdm.trange(args.num_batches):
        opt.zero_grad()

        context = torch.tensor([args.val_batch_size, None], dtype=torch.int32)
        context_in = randomize(context, hparams, args.noise)
        if torch.cuda.is_available():
            context_in.cuda()
        output = model(context_in)

        loss = F.cross_entropy(output.transpose(2, 1), )

        # creates a sample based on the gtp-2 implementation
        pt_sample = sample.sample_sequence(
            model=model,
            hparams=hparams,
            length=args.sample_length,
            context=context,
            batch_size=args.batch_size,
            temperature=1.0,
            top_k=args.top_k,
            top_p=args.top_
        )

        # validating the model
        if args.val_every > 0:
            val_context = torch.tensor([args.val_batch_size, None], dtype=torch.int32)
            val_output = model(val_context)
            val_loss = torch.mean(F.cross_entropy(input=val_context[:, 1:], target=val_output['logits'][:, :-1]))
            print("Validation loss: {}".format(val_loss))
            # vall_loss_summary = summary(val_loss)  muss ich noch korrigieren

        # load the dataset
        print('Loading dataset...')
        chunks = load_dataset(enc, args.dataset, args.combine, encoding=args.encoding)
        data_sampler = Sampler(chunks)
        if args.val_every > 0:
            if args.val_dataset:
                val_chunks = load_dataset(enc, args.val_dataset, args.combine, encoding=args.encoding)
            else:
                val_chunks = chunks
        print('dataset has', data_sampler.total_size, 'tokens')

        # Sample from validation set once with fixed seed to make
        # it deterministic during training as well as across runs.
        if args.val_every > 0:
            val_data_sampler = Sampler(val_chunks, seed=1)
            val_batches = [[val_data_sampler.sample(1024) for _ in range(args.val_batch_size)]
                           for _ in range(args.val_batch_count)]

        # counter for trainingcycle
        counter = 1

        def generate_samples():
            """
            Generate a sample from the predictions of the model to gain insight
            """
            print('Generating samples...')
            context_tokens = data_sampler.sample(1)
            all_text = []
            index = 0
            while index < args.sample_num:
                for i in range(min(args.sample_num - index, args.batch_size)):
                    text = enc.decode(pt_sample[i])
                    text = '======== SAMPLE {} ========\n{}\n'.format(
                        index + 1, text)
                    all_text.append(text)
                    index += 1
            print(text)
            maketree(os.path.join(SAMPLE_DIR, args.run_name))
            with open(
                    os.path.join(SAMPLE_DIR, args.run_name,
                                 'samples-{}').format(counter), 'w', encoding=args.encoding) as fp:
                fp.write('\n'.join(all_text))

        def save(state, is_best):
            """
            Saves the model to be loaded again, if an interruption occurred or to load the model after training separately
            :param state: state of the current model as {epoch:, state_dict:, best_accuracy:} dictionary
            :param is_best: boolean that represents if the models accuracy has improved to verify a useful save
            """
            maketree(os.path.join(CHECKPOINT_DIR, args.run_name))
            if is_best:
                print(
                    'Saving',
                    os.path.join(CHECKPOINT_DIR, args.run_name,
                                 'model-{}').format(counter))
                torch.save(state, os.path.join(CHECKPOINT_DIR, 'model'))
            else:
                print("=> Validation Accuracy did not improve")

        def validation():
            """
            Calculates the validation loss.
            """
            print('Calculating validation loss...')
            losses = []
            for batch in tqdm.tqdm(val_batches):
                losses.append(val_loss)
            v_val_loss = np.mean(losses)
            # v_summary = val_loss_summary
            print(
                '[{counter} | {time:2.2f}] validation loss = {loss:2.2f}'
                .format(
                    counter=counter,
                    time=time.time() - start_time,
                    loss=v_val_loss))

        def sample_batch():
            """
            Generates the sample batch
            :return: return the sample batch as an array
            """
            return [data_sampler.sample(1024) for _ in range(args.batch_size)]

        avg_loss = (0.0, 0.0)
        start_time = time.time()

        opt.step()
        sch.step()



    # creates a summary of the model and prints it
    vgg = model.vgg16()
    summary(vgg, (3, 224, 224))


if __name__ == '__main__':
    options = parser.parse_args(['--dataset', 'data/test_data.txt', '--hparams_path', './'])

    training(options)
