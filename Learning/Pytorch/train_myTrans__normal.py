import argparse
import gc
import glob
import gzip
import json
import math
import os
import random
import sys
import sys
import tarfile
import tqdm
from argparse import ArgumentParser

import numpy as np
import psutil
import seaborn
import torch
import torch.distributions as dist
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable
from torch.utils.tensorboard import SummaryWriter

import encoder
import sample
from MyTransformer import Transformer, default_hparams
from load_dataset import load_dataset, Sampler

# Used for converting between nats and bits
LOG2E = math.log2(math.e)


def dev(tensor=None):
    """
    Returns a device string either for the best available device,
    or for the device corresponding to the argsument
    :param tensor:
    :return:
    """
    if tensor is None:
        return 'cuda' if torch.cuda.is_available() else 'cpu'
    return 'cuda' if tensor.is_cuda else 'cpu'


def here(subpath=None):
    """
    :return: the path in which the package resides (the directory containing the 'former' dir)
    """
    if subpath is None:
        return os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))

    return os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', subpath))


def contains_nan(tensor):
    return bool((tensor != tensor).sum() > 0)


def sample(lnprobs, temperature=1.0):
    """
    Sample an element from a categorical distribution
    :param lnprobs: Outcome log-probabilities
    :param temperature: Sampling temperature. 1.0 follows the given distribution,
        0.0 returns the maximum probability element.
    :return: The index of the sampled element.
    """

    if temperature == 0.0:
        return lnprobs.argsmax()

    p = F.softmax(lnprobs / temperature, dim=0)
    cd = dist.Categorical(p)

    return cd.sample()


def enwik8(path, n_train=int(90e6), n_valid=int(5e6), n_test=int(5e6)):
    """
    Load the enwik8 dataset from the Hutter challenge.
    Adapted from https://github.com/openai/blocksparse/blob/master/examples/transformer/enwik8.py
    :param path:
    :param n_train:
    :param n_valid:
    :param n_test:
    :return:
    """
    with gzip.open(path) if path.endswith('.gz') else open(path) as file:
        X = np.fromstring(file.read(n_train + n_valid + n_test), dtype=np.uint8)
        trX, vaX, teX = np.split(X, [n_train, n_train + n_valid])
        return torch.from_numpy(trX), torch.from_numpy(vaX), torch.from_numpy(teX)


def openwebtext_data(enc, path, combine, encoding='utf-8'):
    """
    This function is using the dataloader drom gpt-2 and its encoder to create chunks of data from a text file
    or multiple files if a directory is given
    :param enc: The encoder to use
    :param path: The path to a text file or a directory of text files
    :param combine: The amount of data that is supposed to be combined
    :param encoding: The encoding type that can be spezified, its default is utf-8
    :return: return the chunks as a torch.tensor
    """
    chunks = np.concatenate(load_dataset(enc, path, combine, encoding=encoding), axis=0)
    chunks_tensor = torch.tensor(chunks)
    return chunks_tensor


def train(args):

    if args.seed < 0:
        seed = random.randint(0, 1000000)
        print('random seed: ', seed)
    else:
        torch.manual_seed(args.seed)

    tbw = SummaryWriter(log_dir=args.tb_dir)  # Tensorboard logging

    # load the data (validation unless args.final is true, then test)
    # args.data = here('data/enwik8.gz') if args.data is None else args.data
    #
    # data_train, data_val, data_test = enwik8(args.data)
    # data_train, data_test = (torch.cat([data_train, data_val], dim=0), data_test) \
    #                         if args.final else (data_train, data_val)

    # chunks are used as traindings data
    # same goes for the val_chunks
    print("### Encoding... ###")
    enc = encoder.get_encoder(args.encoder_path, args.vocab_path)
    chunks = openwebtext_data(enc, args.dataset, args.combine, args.encoding)
    print(chunks.size())
    if args.val_every > 0:
        if args.val_dataset:
            val_chunks = load_dataset(enc, args.val_dataset, args.combine, encoding=args.encoding)
        else:
            val_chunks = chunks
        # Sample from validation set once with fixed seed to make
        # it deterministic during training as well as across runs.
        val_data_sampler = Sampler(val_chunks, seed=1)
        val_batches = [[val_data_sampler.sample(1024) for _ in range(args.val_batch_size)]
                       for _ in range(args.val_batch_count)]
    print("### Creating datasets ###")
    if args.test_dataset:
        test_chunks = load_dataset(enc, args.test_dataset, args.combine, encoder=args.encoding)
    elif args.test_size > 0:
        train_part = chunks.size() * (1.0 - args.test_size)
        test_chunks = chunks[train_part:]
        chunks = chunks[:train_part]
    else:
        train_part = int(float(chunks.size()[0]) * 0.95)
        print(train_part)
        test_chunks = chunks[train_part:]
        chunks = chunks[:train_part]

    print('### dataset has', chunks.size(), 'tokens ###')

    # loads the hparams from a json, if specified
    if args.hparams_path != "":
        with open(args.hparams_path) as f:
            hparams = json.load(f)
            print("Using specified hparameters: {}".format(hparams))
    else:
        hparams = default_hparams()
        print("Using default hparameters: {}".format(hparams))
    # create the model
    model = Transformer(hparams=hparams)
    if not torch.cuda.is_available():
        print("The model will be trained on the GPU.")
        print(torch.cuda.get_device_name())
        model.cuda()

    opt = torch.optim.Adam(lr=args.lr, params=model.parameters())
    # linear learning rate warmup
    sch = torch.optim.lr_scheduler.LambdaLR(opt, lambda i: min(i / (args.lr_warmup / args.batch_size), 1.0))

    losses = []
    print("### Training... ###")
    # training loop
    # - note: we don't loop over the data, instead we sample a batch of random subsequences each time.
    for i in tqdm.trange(args.num_batches):
        gc.collect()
        opt.zero_grad()

        # sample a batch of random subsequences
        ctx = hparams["n_ctx"]
        starts = torch.randint(size=(args.batch_size, ), low=0, high=chunks.size(0) - ctx - 1)
        seqs_source = [chunks[start:start+ctx] for start in starts]
        seqs_targset = [chunks[start+1:start+ctx+1] for start in starts]
        source = torch.cat([s[None, :] for s in seqs_source], dim=0).to(torch.long)
        target = torch.cat([s[None, :] for s in seqs_targset], dim=0).to(torch.long)
        # - target is the same sequence as source, except one character ahead

        if not torch.cuda.is_available():
            source, target = source.cuda(), target.cuda()
        source, target = Variable(source), Variable(target)

        output = model(source)
        loss = F.cross_entropy(output.transpose(2, 1), target, reduction='mean')  # test if this one works better
        # loss = F.nll_loss(output.transpose(2, 1), target, reduction='mean')
        bat = i * args.batch_size
        losses.append([loss.item(), bat])

        tbw.add_scalar('transformer/train-loss', float(loss.item()) * LOG2E, i * args.batch_size)

        loss.backward()

        # clip gradients
        # - If the total gradient vector has a length > 1, we clip it back down to 1.
        if args.gradient_clipping > 0.0:
            nn.utils.clip_grad_norm_(model.parameters(), args.gradient_clipping)

        opt.step()
        sch.step()

        # - validate every {args.test_every} steps. First we compute the
        #   compression on the validation (or a subset)
        #   then we generate some random text to monitor progress
        if i != 0 and (i % args.test_every == 0 or i == args.num_batches - 1):

            upto = test_chunks.size(0) if i == args.num_batches - 1 else args.test_subset
            data_sub = test_chunks[:upto]

            with torch.no_grad():
                bits, tot = 0.0, 0
                batch = [] # buffer, every time it fills up, we run it through the model

                for current in range(data_sub.size(0)):

                    fr = max(0, current - args.context)
                    to = current + 1

                    context = data_sub[fr:to].to(torch.long)
                    if context.size(0) < args.context + 1:
                        pad = torch.zeros(size=(args.context + 1 - context.size(0),), dtype=torch.long)
                        context = torch.cat([pad, context], dim=0)

                        assert context.size(0) == args.context + 1

                    if torch.cuda.is_available():
                        context = context.cuda()

                    batch.append(context[None, :])

                    if len(batch) == args.test_batchsize or current == data_sub.size(0) - 1:

                        # batch is full, run it through the model
                        b = len(batch)

                        all = torch.cat(batch, dim=0)
                        source = all[:, :-1] # input
                        targset = all[:, -1]  # targset values

                        output = model(source)

                        lnprobs = output[torch.arange(b, device=dev()), -1, targset]
                        log2probs = lnprobs * LOG2E # convert from nats to bits

                        bits += - log2probs.sum()
                        batch = [] # empty buffer

                bits_per_byte = bits / data_sub.size(0)

                # print validation performance. 1 bit per byte is (currently) state of the art.
                print(f'epoch{i}: {bits_per_byte:.4} bits per byte')
                tbw.add_scalar(f'transformer/eval-loss', bits_per_byte, i * args.batch_size)

                # generate some random text
                GENSIZE = 250
                TEMP = 0.5
                seedfr = random.randint(0, test_chunks.size(0) - args.context)
                input = test_chunks[seedfr:seedfr + args.context].to(torch.long)

                if torch.cuda.is_available():
                    input = input.cuda()

                input = Variable(input)

                print('[', end='', flush=True)
                for c in input:
                    print(str(chr(c)), end='', flush=True)
                print(']', end='', flush=True)

                for _ in range(GENSIZE):
                    output = model(input[None, :])
                    c = sample(output[0, -1, :], TEMP)
                    print(str(chr(max(32, c))), end='', flush=True)

                    input = torch.cat([input[1:], c[None]], dim=0)

                print()
    print("Saving the model.")
    torch.save(model.state_dict(), 'random_model')
    print("Model saved")

    print("Saving the models training performance.")
    with open("evaluation_infos_random_model.txt", "w") as txt_file:
        txt_file.write("Losses: \n")
        for line in losses:
            txt_file.write(" ".join(str(line)) + "\n")
        txt_file.write("\n Compression: \n")
    print("Information file written.")


if __name__ == "__main__":
    # creates a parser to specify the training information
    parser = argparse.ArgumentParser(
        description='Train generate-transformer on a custom dataset.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # adding the different specification options and defining default values
    parser.add_argument('--dataset', metavar='PATH', type=str, default="data/test_data.txt", required=True, help='Input file, directory, or glob pattern (utf-8 text, or preencoded .npz files).')
    parser.add_argument('--combine', metavar='CHARS', type=int, default=50000, help='Concatenate input files with <|endoftext|> separator into chunks of this minimum size')
    parser.add_argument('--encoder_path', default="encoder.json", help="This is the path to the encoder.json file to definde the encoder.")
    parser.add_argument('--encoding', type=str, default='utf-8', help='Set the encoding for reading and writing files.')
    parser.add_argument('--vocab_path', default="vocab.bpe", help="This is the path to the vocab.bpe file to definde the vocabulary.")
    parser.add_argument('--hparams_path', type=str, default="", help="The path to the hyper parameters directory.")

    parser.add_argument('--batch_size', metavar='SIZE', type=int, default=1, help='Batch size')
    parser.add_argument("-N", "--num-batches", dest="num_batches", help="Number of batches to train on. Each batch contains randomly sampled subsequences of the data.", default=60_000, type=int)
    parser.add_argument('--learning_rate', dest="lr", metavar='LR', type=float, default=0.00002, help='Learning rate for Adam')
    parser.add_argument('--lr-warmup', dest="lr_warmup", default=5000, type=int, help="Learning rate warmup.")
    parser.add_argument('--optimizer', type=str, default='adam', help='Optimizer. <adam|sgd>.')
    parser.add_argument('--noise', type=float, default=0.0, help='Add noise to input training data to regularize against typos.')

    parser.add_argument('--sample_every', metavar='N', type=int, default=100, help='Generate samples every N steps')
    parser.add_argument('--sample_length', metavar='TOKENS', type=int, default=1023, help='Sample this many tokens')
    parser.add_argument('--seed', type=int, default=1, help='RNG seed. Negative for random')

    parser.add_argument('--val_dataset', metavar='PATH', type=str, default=None, help='Dataset for validation loss, defaults to --dataset.')
    parser.add_argument('--val_batch_size', metavar='SIZE', type=int, default=2, help='Batch size for validation.')
    parser.add_argument('--val_batch_count', metavar='N', type=int, default=40, help='Number of batches for validation.')
    parser.add_argument('--val_every', metavar='STEPS', type=int, default=0, help='Calculate validation loss every STEPS steps.')

    parser.add_argument('--test_dataset', metavar='PATH', type=str, default=None, help='Dataset for test loss, defaults to --dataset.')
    parser.add_argument('--test_batch_size', metavar='SIZE', type=int, default=2, help='Batch size for testing.')
    parser.add_argument('--test_batch_count', metavar='N', type=int, default=40, help='Number of batches for testing.')
    parser.add_argument('--test_size', metavar='STEPS', type=int, default=0, help='To define the amount of data from the trainings_set to use for testing.')

    parser.add_argument('--tb_dir', default='./runs', dest='tb_dir', help='Tensorboard logging directory')

    options = parser.parse_args(['--dataset', 'enwik8.gz', '--hparams_path', './hparams_bert_vocab.json'])
    print("Options: {}".format(options))
    train(options)

