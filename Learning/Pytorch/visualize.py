import math, random, gzip, os
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import glob
import ast

import torch
import torch.nn.functional as F
from PBloemTransformers import GTransformer
import torch.distributions as dist
from torch.autograd import Variable

LOG2E = math.log2(math.e)

should_plot = False
matplot = False
seaborn = True


def remove_spaces(input_string):
    """
    Does what the name says, removes spaces from a string
    @param input_string: string in which the witespaces are supposed to be removed
    @return returns: the string without spaces
    """
    return "".join(input_string.split())


def d(tensor=None):
    """
    Returns a device string either for the best available device,
    or for the device corresponding to the argument
    :param tensor:
    :return:
    """
    if tensor is None:
        return 'cuda' if torch.cuda.is_available() else 'cpu'
    return 'cuda' if tensor.is_cuda else 'cpu'


def get_file_infos(path, old=False):
    """
    Retrieves the information for visualisation from a given file
    @param path: A string containing the path to the file from which the information will be extracted
    @return: returns a compression, loss and batch array
    """
    compress_arr = []
    loss_arr = []
    batch_arr = []
    accuracy_arr = []
    epoch_accuracy_avg_arr = []
    epoch_arr = []
    if old:
        # This one is only for the standard model, since its evaluation info was written in a different format
        with open(path, 'r') as fs:
            is_compression = False
            for row in fs.readlines():
                if row[0] == 'C':
                    is_compression = True
                if is_compression and row[0] == 't':
                    row = remove_spaces(row)
                    comp = row.split('(')[-1].split(',')[0]
                    compress_arr.append(comp)
                elif row[0] == 't':
                    row = remove_spaces(row)
                    loss = float(row.split('(')[-1].split(',')[0]) * LOG2E
                    loss_arr.append(loss)
    else:
        m = 0
        with open(path, 'r') as fs:
            is_compression = False
            is_accuracy = False
            for row in fs.readlines():
                if row[0] == 'C':
                    is_compression = True
                if is_compression and row[0] == 't':
                    row = remove_spaces(row)
                    comp = row.split('(')[-1].split(',')[0]
                    compress_arr.append(comp)
                if row[0] == "M":
                    is_accuracy = True
                if is_accuracy and row[0] == "[":
                    acc = "["+row.split("[")[-1].split("]")[0]+"]"
                    acc = ast.literal_eval(acc)
                    epo = int(row.split(", ")[-1].split("]")[0])
                    epo_acc_avg = np.mean(acc)
                    epoch_accuracy_avg_arr.append(epo_acc_avg)
                    accuracy_arr.append(acc)
                    epoch_arr.append(epo)
                elif row[0] == '[':
                    m+=1
                    row = remove_spaces(row)
                    bat = int(row.split(',')[-1].split(']')[0])
                    loss = float(row.split('[')[-1].split(',')[0]) * LOG2E
                    loss_arr.append(loss)
                    batch_arr.append(bat)
    return compress_arr, loss_arr, batch_arr, accuracy_arr, epoch_accuracy_avg_arr, epoch_arr


def get_average(mody_arr, norm_arr, save_path, avg_over):
    """
    This function calculates the average of a loss function and prepares the datat to be used in a pandas dataframe
    @param losses_mody: Array containing the loss function of the modified model
    @param losses_norm: Array containing the loss function of the standard model
    @param save_path: A string that defines the name of the file to save it as
    @return: returns the average loss function, the batches for said loss function and the model type array for seaborn plotting
    """
    mody_avg_arr = np.array([])
    norm_avg_arr = np.array([])
    mody = np.array([], dtype=np.str)
    norm = np.array([], dtype=np.str)
    j = 0
    ml = len(mody_arr)
    nl = len(norm_arr)
    if ml > nl != 0:
        diff = ml - nl
        new_end = ml - diff
        mody_arr = mody_arr[:new_end]
    elif nl > ml != 0:
        diff = nl - ml
        new_end = nl - diff
        norm_arr = norm_arr[:new_end]

    ep = np.arange(0, len(mody_arr), avg_over)
    if not norm_arr:
        epochs_for_avg = np.arange(0, len(mody_arr), avg_over)
        for i in range(0, len(mody_arr), avg_over):
            if i == 0:
                mody_avg_arr = np.append(mody_avg_arr, mody_arr[0])
            else:
                mody = np.append(mody, "mody")
                sum_mody = np.sum(mody_arr[j:i])
                av_mody = sum_mody / avg_over
                mody_avg_arr = np.append(mody_avg_arr, av_mody)
            j = i
    elif not mody_arr:
        epochs_for_avg = np.arrange(0, len(norm_arr), avg_over)
        for i in range(0, len(norm_arr), avg_over):
            norm = np.append(norm, "norm")
            if i == 0:
                norm_avg_arr = np.append(norm_avg_arr, norm_arr[0])
            else:
                sum_norm = np.sum(norm_arr[j:i])
                av_norm = sum_norm / avg_over
                norm_avg_arr = np.append(norm_avg_arr, av_norm)
            j = i
    else:
        ep = np.arange(0, len(mody_arr), avg_over)
        for i in range(0, len(norm_arr), avg_over):
            mody = np.append(mody, "mody")
            norm = np.append(norm, "norm")
            if i == 0:
                mody_avg_arr = np.append(mody_avg_arr, mody_arr[0])
                norm_avg_arr = np.append(norm_avg_arr, norm_arr[0])
            else:
                sum_norm = np.sum(norm_arr[j:i])
                av_norm = sum_norm / avg_over
                norm_avg_arr = np.append(norm_avg_arr, av_norm)
                sum_mody = np.sum(mody_arr[j:i])
                av_mody = sum_mody / avg_over
                mody_avg_arr = np.append(mody_avg_arr, av_mody)
            j = i
        epochs_for_avg = np.concatenate((ep, ep), axis=None)

    with open("Evaluation/Info_files/Average/loss_{}.txt".format(save_path), "w") as f:
        for i in range(len(mody_avg_arr)):
            if mody_avg_arr.size == 0:
                f.write("[" + str(norm_avg_arr[i]) + "]\n")
            elif norm_avg_arr.size == 0:
                f.write("[" + str(mody_avg_arr[i]) + "]\n")
            else:
                f.write("[" + str(mody_avg_arr[i]) + ", " + str(norm_avg_arr[i]) + "]\n")
        f.close()

    loss_avg = np.concatenate((mody_avg_arr, norm_avg_arr), axis=None)
    model_type = np.concatenate((mody, norm), axis=None)
    return loss_avg, epochs_for_avg, model_type, mody_avg_arr, norm_avg_arr, ep


def get_average_2d(mody_arr, norm_arr, save_path, avg_over):
    """
    Calculate the average of the given 2 dim arrays and stores them in an array ready to be visulized
    @param mody_arr: the 2dim array of the modified model (probably the accuracy)
    @param norm_arr: the 2dim array of the standard model (probably the accuracy)
    @param save_path: the location where the calculated average information will be saved
    @param avg_over: the amount of values over which the average should be calculated
    @return: returns an array containing the average of norm and mody, another array of epochs for plotting and
    the model types
    """
    mody_avg_arr = np.array([])
    norm_avg_arr = np.array([])
    mody = np.array([], dtype=np.str)
    norm = np.array([], dtype=np.str)
    mody_avg_arr_all_epochs = np.array([])
    norm_avg_arr_all_epochs = np.array([])
    j = 0

    if len(mody_arr) > 0 and len(norm_arr) > 0:
        mody_avg_arr_all_epochs = np.mean(mody_arr, axis=1).flatten()
        norm_avg_arr_all_epochs = np.mean(norm_arr, axis=1).flatten()
        ep = np.arange(0, len(norm_avg_arr_all_epochs), avg_over)
    elif len(mody_arr) > 0:
        mody_avg_arr_all_epochs = np.mean(mody_arr, axis=1).flatten()
        ep = np.arange(0, len(mody_avg_arr_all_epochs), avg_over)
    else:
        norm_avg_arr_all_epochs = np.mean(norm_arr, axis=1).flatten()
        ep = np.arange(0, len(norm_avg_arr_all_epochs), avg_over)

    ml = mody_avg_arr_all_epochs.size
    nl = norm_avg_arr_all_epochs.size
    if ml > nl != 0:
        diff = ml - nl
        new_end = ml - diff
        mody_avg_arr_all_epochs = mody_avg_arr_all_epochs[:new_end]
    elif nl > ml != 0:
        diff = nl - ml
        new_end = nl - diff
        norm_avg_arr_all_epochs = norm_avg_arr_all_epochs[:new_end]

    print(mody_avg_arr_all_epochs.size, norm_avg_arr_all_epochs.size)

    if mody_avg_arr_all_epochs.size == 0:
        for i in range(0, len(norm_avg_arr_all_epochs), avg_over):
            if i == 0:
                np.append(norm_avg_arr, norm_avg_arr_all_epochs[0])
            else:
                norm = np.append(norm, "norm")
                sum_norm = np.sum(norm_avg_arr_all_epochs[j:i])
                av_norm = sum_norm / avg_over
                norm_avg_arr = np.append(norm_avg_arr, av_norm)
            j = i
    elif norm_avg_arr_all_epochs.size == 0:
        for i in range(0, len(mody_avg_arr_all_epochs), avg_over):
            if i == 0:
                np.append(mody_avg_arr, mody_avg_arr_all_epochs[0])
            else:
                mody = np.append(mody, "mody")
                sum_rand = np.sum(mody_avg_arr_all_epochs[j:i])
                av_mody = sum_rand / avg_over
                mody_avg_arr = np.append(mody_avg_arr, av_mody)
            j = i
    else:
        for i in range(0, len(norm_avg_arr_all_epochs), avg_over):
            if i == 0:
                norm_avg_arr = np.append(norm_avg_arr, norm_avg_arr_all_epochs[0])
                mody_avg_arr = np.append(mody_avg_arr, mody_avg_arr_all_epochs[0])
            else:
                norm = np.append(norm, "norm")
                mody = np.append(mody, "mody")
                sum_norm = np.sum(norm_avg_arr_all_epochs[j:i])
                av_norm = sum_norm / avg_over
                norm_avg_arr = np.append(norm_avg_arr, av_norm)
                sum_rand = np.sum(mody_avg_arr_all_epochs[j:i])
                av_mody = sum_rand / avg_over
                mody_avg_arr = np.append(mody_avg_arr, av_mody)
            j = i

    with open("Evaluation/Info_files/Average/accuracy_{}.txt".format(save_path), "w") as f:
        for i in range(len(mody_avg_arr)):
            if mody_avg_arr.size == 0:
                f.write("[" + str(norm_avg_arr[i]) + "]\n")
            elif norm_avg_arr.size == 0:
                f.write("[" + str(mody_avg_arr[i]) + "]\n")
            else:
                f.write("[" + str(mody_avg_arr[i]) + ", " + str(norm_avg_arr[i]) + "]\n")
        f.close()
        print("### Information written. ###")

    acc_avg = np.concatenate((mody_avg_arr, norm_avg_arr), axis=None)
    model_type = np.concatenate((mody, norm), axis=None)
    return acc_avg, model_type, mody_avg_arr, norm_avg_arr, ep


def visualize(path_mody="", path_norm="", matplot=False, seaborn= False, saving_path=""):
    """
    A simple function to create a plot that is able to create a comparison plot between standard and modified
    @param path_mody: path to the modified models training information
    @param path_norm: path to the standard models training information
    @param matplot: set to true if the plot should be done through matplot
    @param seaborn: set to true if the plot should be done through seaborn
    """

    x_steps = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0]
    if path_mody != "":
        compressions_mody, losses_mody, batches, accuracy_mody, accuracy_epoch_avg, epochs = get_file_infos(path_mody)
    else:
        compressions_mody, losses_mody, batches, accuracy_mody, accuracy_epoch_avg, epochs = [], [], [], [], [], []
    if path_norm != "":
        compressions_norm, losses_norm, batches, accuracy_norm, accuracy_epoch_avg, epochs = get_file_infos(path_norm)
    elif path_norm == "" and batches == []:
        compressions_norm, losses_norm, batches, accuracy_norm, accuracy_epoch_avg, epochs = [], [], [], [], [], []
    else:
        compressions_norm, losses_norm, accuracy_norm, accuracy_epoch_avg, epochs = [], [], [], [], []

    loss_avg, epochs_for_avg, model_type_loss, loss_avg_mody, loss_avg_norm, epochs = get_average(losses_mody, losses_norm, saving_path, 250)
    accuracy_avg, model_type_acc, acc_avg_mody, acc_avg_norm, epochs = get_average_2d(accuracy_mody, accuracy_norm, saving_path, 250)

    data = {'Loss_mody': losses_mody,
            'Loss_norm': losses_norm,
            'Loss_avg': loss_avg,
            "Loss_avg_norm": loss_avg_norm,
            "Loss_avg_mody": loss_avg_mody,
            'Batches': batches,
            'Epochs_for_avg': epochs_for_avg,
            'Model_type_loss': model_type_loss,
            "Model_type_acc": model_type_acc,
            "Accuracy_mody": accuracy_mody,
            "Accuracy_norm": accuracy_norm,
            "Accuracy_avg": accuracy_avg,
            "Accuracy_avg_mody": acc_avg_mody,
            "Accuracy_avg_norm": acc_avg_norm,
            "Epochs": epochs}
    print(loss_avg.size, accuracy_avg.size)
    if 0 < loss_avg.size == accuracy_avg.size > 0:
        print(len(data["Loss_avg"]), len(data["Accuracy_avg"]), len(data["Epochs_for_avg"]), len(data["Model_type_acc"]), len(data["Model_type_loss"]))
        df = pd.DataFrame(data, columns=["Accuracy_avg_mody", "Accuracy_avg_norm", "Epochs", "Loss_avg_norm", "Loss_avg_mody"])
    elif loss_avg.size > 0:
        print(len(data["Loss_avg"]), len(data["Epochs_for_avg"]), len(data["Model_type_loss"]))
        df = pd.DataFrame(data, columns=['Loss_avg', 'Epochs_for_avg', 'Model_type_loss'])
    else:
        print(len(data["Accuracy_avg"]), len(data["Epochs_for_avg"]), len(data["Model_type_acc"]))
        df = pd.DataFrame(data, columns=['Accuracy_avg', 'Epochs_for_avg', 'Model_type_acc'])

    print(df)

    if matplot:
        print("### Plotting via Matplot ###")
        # working with pyplot
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(15, 10))
        ax1.plot(losses_mody, linewidth=0.5)
        ax1.set(xlabel="Epochs", ylabel="Loss", yticks=x_steps,
               title="Trainings-loss of the model (negative log likelihood loss-function)")
        ax1.grid()
        ax2.plot(compressions_mody, linewidth=0.5)
        ax2.set(xlabel="Epoch (every 5000)", ylabel="Compression (bits per bytes)",
                title="Compression capabilities of the model, as a performance test.")
        ax2.grid()
        plt.savefig("Evaluation/Plots/{}.png".format(saving_path))
        print("### Figure saved ###")
    elif seaborn:
        print("### Plotting via seaborn ###")
        sns.set_theme(style="darkgrid")
        if accuracy_avg.size <= 241:
            print("### Only loss plotable ###")
            line, graph = plt.subplots(figsize=(20, 10))
            graph = sns.FacetGrid(df, col="Model_type_loss", height=10)
            graph.map(sns.lineplot, "Epochs_for_avg", "Loss_avg", markers=True, dashes=False)
            graph.add_legend()
            graph.fig.subplots_adjust(top=0.9)
            graph.fig.suptitle(
                "Trainings-Loss of both models (loss-function: negative log likelihood loss-function)", fontsize=16)
            graph.savefig("Evaluation/Plots/{}.png".format(saving_path))
            print("### Figure saved ###")
        elif loss_avg.size <= 241:
            print("##ä Only accuracy plotable ###")
            line, graph = plt.subplots(figsize=(20, 10))
            graph = sns.FacetGrid(df, col="Model_type_acc", height=10)
            graph.map(sns.lineplot, "Epochs_for_avg", "Accuracy_avg", markers=True, dashes=False)
            graph.add_legend()
            graph.fig.subplots_adjust(top=0.9)
            graph.fig.suptitle(
                "Trainings-Accuracy of both models", fontsize=16)
            graph.savefig("Evaluation/Plots/{}.png".format(saving_path))
            print("### Figure saved ###")
        else:
            print("### loss and accuracy plotted ###")
            fig, axes = plt.subplots(2, 2, figsize=(20, 10))
            sns.lineplot(data=df, x="Epochs", y="Accuracy_avg_mody", ax=axes[0, 0], color="green").set_title("Modified model accuracy")
            sns.lineplot(data=df, x="Epochs", y="Accuracy_avg_norm", ax=axes[0, 1], color="green").set_title("Standard model accuracy")
            sns.lineplot(data=df, x="Epochs", y="Loss_avg_mody", ax=axes[1, 0]).set_title("Modified model loss")
            sns.lineplot(data=df, x="Epochs", y="Loss_avg_norm", ax=axes[1, 1]).set_title("Standard model loss")
            fig.savefig("Evaluation/Plots/{}.png".format(saving_path))
            print("### Figure saved ###")
    # plt.show()


def here(subpath=None):
    """
    :return: the path in which the package resides (the directory containing the 'former' dir)
    """
    if subpath is None:
        return os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))

    return os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', subpath))


def sample(lnprobs, temperature=1.0):
    """
    Sample an element from a categorical distribution
    :param lnprobs: Outcome log-probabilities
    :param temperature: Sampling temperature. 1.0 follows the given distribution,
        0.0 returns the maximum probability element.
    :return: The index of the sampled element.
    """

    if temperature == 0.0:
        return lnprobs.argmax()

    p = F.softmax(lnprobs / temperature, dim=0)
    cd = dist.Categorical(p)

    return cd.sample()


def enwik8(path, n_train=int(90e6), n_valid=int(5e6), n_test=int(5e6)):
    """
    Load the enwik8 dataset from the Hutter challenge.
    Adapted from https://github.com/openai/blocksparse/blob/master/examples/transformer/enwik8.py
    :param path:
    :param n_train:
    :param n_valid:
    :param n_test:
    :return:
    """
    with gzip.open(path) if path.endswith('.gz') else open(path) as file:
        X = np.frombuffer(file.read(n_train + n_valid + n_test), dtype=np.uint8)
        trX, vaX, teX = np.split(X, [n_train, n_train + n_valid])
        return torch.from_numpy(trX), torch.from_numpy(vaX), torch.from_numpy(teX)


def generate_from_data(data_path, model, gensize, temp):
    """
    A function that uses the trained model to generate text from a start string taken randomly from a given file
    @param data_path: path to the data from which the starting string will be taken
    @param model: the trained model
    @param gensize: the length of the sample the model will be generating
    @param temp: the temperature value
    """
    data = here(data_path)
    data_train, data_val, data_test = enwik8(data)
    seedfr = random.randint(0, data_test.size(0) - 256)
    input = data_test[seedfr:seedfr + 256].to(torch.long)

    if torch.cuda.is_available():
        model.cuda()
        input = input.cuda()
        print(torch.cuda.get_device_name())
    else:
        print("Could not use GPU, as none was found")

    input = Variable(input)

    print('[', end='', flush=True)
    for c in input:
        print(str(chr(c)), end='', flush=True)
    print(']', end='', flush=True)

    print("\n ### Generating sample ### \n")
    gen_string = ""
    for _ in range(gensize):
        output = model(input[None, :])
        c = sample(output[0, -1, :], temp)
        print(str(chr(max(32, c))), end='', flush=True)
        gen_string += str(chr(max(32, c)))

        input = torch.cat([input[1:], c[None]], dim=0)

    return gen_string


def generate_from_data_multiple(data_path, model, gensize, temp, sample_len):
    """
    A function that uses the trained model to generate text from a start string taken randomly from a given file
    The print statements have been removed to enable a smoother generation of multiple samples
    @param data_path: path to the data from which the starting string will be taken
    @param model: the trained model
    @param gensize: the length of the sample the model will be generating
    @param temp: the temperature value
    """
    data = here(data_path)
    data_train, data_val, data_test = enwik8(data)
    seedfr = random.randint(0, data_test.size(0) - sample_len)
    input = data_test[seedfr:seedfr + sample_len].to(torch.long)

    if torch.cuda.is_available():
        model.cuda()
        input = input.cuda()
    else:
        print("Could not use GPU, as none was found")

    input = Variable(input)

    gen_string = ""
    for _ in range(gensize):
        output = model(input[None, :])
        c = sample(output[0, -1, :], temp)
        gen_string += str(chr(max(32, c)))

        input = torch.cat([input[1:], c[None]], dim=0)

    return gen_string


def generate_from_string(string, model, gensize, temp, sample_len):
    """
    The function to generate text from a given string
    @param string: the string the model will be generating text from
    @param model: the trained model
    @param gensize: the length of the generated string
    @param temp: the temperature value
    """
    string = torch.from_numpy(np.frombuffer(bytes(string, 'utf-8'), dtype=np.uint8))
    input = string[0:sample_len].to(torch.long)
    if torch.cuda.is_available():
        model.cuda()
        input = input.cuda()
        print(torch.cuda.get_device_name())
    else:
        print("Could not use GPU, as none was found")

    input = Variable(input)
    print("[", end='', flush=True)
    for c in input:
        print(str(chr(c)), end='', flush=True)
    print(']', end='', flush=True)

    print("\n ### Generating sample ### \n")

    gen_string = ""
    for _ in range(gensize):
        output = model(input[None, :])
        c = sample(output[0, -1, :], temp)
        print(str(chr(max(32, c))), end='', flush=True)
        gen_string += str(chr(max(32, c)))

        input = torch.cat([input[1:], c[None]], dim=0)

    return gen_string

def compress_text(text, model, context, batch_size):
    """
    The function to text the compression capabilities of the model
    @param text: the text that will be compressed
    @param model: the trained model
    @param context: the context length
    @param batch_size: the batch size
    @return: returns a float representing the amount of bits needed for a byte
    """
    bits, tot = 0.0, 0
    batch = []  # buffer, every time it fills up, we run it through the model

    for current in range(text.size(0)):

        fr = max(0, current - context)
        to = current + 1

        context = text[fr:to].to(torch.long)
        if context.size(0) < context + 1:
            pad = torch.zeros(size=(context + 1 - context.size(0),), dtype=torch.long)
            context = torch.cat([pad, context], dim=0)

            assert context.size(0) == context + 1

        if torch.cuda.is_available():
            context = context.cuda()

        batch.append(context[None, :])

        if len(batch) == batch_size or current == text.size(0) - 1:
            # batch is full, run it through the model
            b = len(batch)

            all = torch.cat(batch, dim=0)
            source = all[:, :-1]  # input
            target = all[:, -1]  # target values

            output = model(source)

            lnprobs = output[torch.arange(b, device=d()), -1, target]
            log2probs = lnprobs * LOG2E  # convert from nats to bits

            bits += - log2probs.sum()
            batch = []  # empty buffer

    bits_per_byte = bits / text.size(0)
    return bits_per_byte


if __name__ == '__main__':
    data_path = "Learning/Pytorch/data/enwik8.gz"

    plotting = True
    generating = False
    generate_multiple = False
    display_paramet_count = True

    # generate some random text
    GENSIZE = 256
    TEMP = 0.5
    state_dic = "model/trained_models/standard_model"
    seq_len = 256

    # loading trained model
    model = GTransformer(emb=128, heads=8, depth=8, seq_length=seq_len,
                         num_tokens=256, wide=True, random=True)
    model.load_state_dict(torch.load(state_dic))
    print("### Model loaded ###")


    string = "In his ''The Ego and Its Own'' Stirner argued that most commonly accepted social institutions - including the notion of State, property as a right, natural rights in general, and the very notion of society - were mere illusions or ''ghosts'' in the mind, saying of society  that &quot;the individuals are its reality.&quot; He advocated egoism and a form of amoralism, in which individuals would unite in 'associations of egoists' only when it was in their self interest to do so.  For him, property simply comes about through might: &quot;Whoever knows how to take, to defend, the thing, to him belongs property.&quot; And, &quot;What I have in my power, that is my own. So long as I assert myself as holder, I am the proprietor of the thing.&quot;"
    short_string = "In his ''The Ego and Its Own'' Stirner argued that most commonly accepted social institutions"

    if plotting:
        path_norm = ""
        plot_all = True
        direct = "Evaluation/info_files/"
        if plot_all:
            for txt in glob.glob("Evaluation/info_files/*.txt"):
                eval = txt.split("\\")[-1]
                if not os.path.isfile("Evaluation/Plots/{}.png".format(eval.split("infos_")[-1].split(".")[0])):
                    print(eval)
                    path_mody = direct + eval
                    path_norm = "{}evaluation_infos_standard_model_context_32_b28_done.txt".format(direct)
                    saving_path = eval.split("infos_")[-1].split(".")[0]
                    visualize(path_mody, path_norm, seaborn=True, saving_path=saving_path)
        else:
            path_norm = "{}evaluation_infos_standard_model_context_32_b28_done.txt".format(direct)
            path_mody = "{}evaluation_infos_mody_model_context_32_m5_b28_first10_moving_letter_replacement_first_block_mask_done.txt".format(direct)
            # path_mody = ""
            saving_path = "std_mody_model"
            visualize(path_mody, path_norm, seaborn=True, saving_path=saving_path)
    if generating:
        generate_from_string(string, model, GENSIZE, TEMP, sample_len=256)
        # test = generate_from_data(data_path, model, GENSIZE, TEMP)
        # print("teststring is: " + test)
    elif generate_multiple:
        with open("Evaluation/example_generations.txt", "w", "utf-8") as f:
            print("### example generation file will be created ###")
            for path in glob.glob("model/trained_models/*"):
                model_path = path.split("\\")[-1]
                seq = int(model_path.split("context_")[-1].split("_")[0])
                sample_len = seq + 20
                model = GTransformer(emb=128, heads=8, depth=8, seq_length=seq,
                                 num_tokens=256, wide=True, random=False)
                model.load_state_dict(torch.load("model/trained_models/{}".format(model_path)))
                print("### Model ({}) loaded ###".format(model_path))
                f.write("{}: \n".format(model_path))
                for i in range(0, 5):
                    string = generate_from_data_multiple(data_path, model, GENSIZE, TEMP, sample_len=sample_len)
                    f.write(string + "\n")
        print("### example generations file creation is done ###")
    if display_paramet_count:
        total_params = []
        for param in model.parameters():
            param_size = param.size()
            total_params.append(np.sum(param_size))
        total_params = np.sum(total_params)
        print(total_params)
