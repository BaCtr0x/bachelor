import json
"""
A simple function to create the encoder.json file for the model, as it is based on the gpt-2 model, which has an encoder
that needs such a file.
"""


path = "model/bert_small/vocab.txt"

dict1 = {}

with open(path, encoding='utf-8') as fh:
    i = 0
    for token in fh.readlines():
        # to ignore the unused tokens, that will only clutter the vocabulary.
        if token[:7] != "[unused":
            str_token = token.split('\n')[0]
            dict1[str_token] = i
            i += 1

output_file = open("bert_encoder.json", "w")
json.dump(dict1, output_file, indent=4, sort_keys=False)
output_file.close()
