import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

sns.set_theme(style="darkgrid")
tips = sns.load_dataset("tips")
print(tips)
fmri = sns.load_dataset("fmri")
# sns.relplot(x="timepoint", y="signal", kind="line", ci="sd", data=fmri)
g = sns.FacetGrid(tips, col="time")
g.map(sns.histplot, "tip")

plt.show()
