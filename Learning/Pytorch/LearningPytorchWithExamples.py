# -*- coding: utf-8 -*-
import torch
import random

"""
We can implement our own custom autograd Functions by subclassing
torch.autograd.Function and implementing the forward and backward passes
which operate on Tensors.
"""


class MyReLu(torch.autograd.Function):

    @staticmethod
    def forward(ctx, input):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        ctx.save_for_backward(input)
        return input.clamp(min=0)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        input, =ctx.saved_tensors
        grad_input = grad_output.clone()
        grad_input[input < 0] = 0
        return grad_input


class TwoLayerNet(torch.nn.Module):
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linaer modules and assign them as member variables.
        :param D_in:
        :param H:
        :param D_out:
        """
        super(TwoLayerNet, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.linear2 = torch.nn.Linear(H, D_out)

    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return a Tensor of output data. We can use
        Modules defined in the constructor as well as arbitrary operators on Tensors.
        :param x:
        :return:
        """
        h_relu = self.linear1(x).clamp(min=0)
        y_pred = self.linear2(h_relu)
        return y_pred


class DynamicNet(torch.nn.Module):
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we construct three nn.Linear instances that we will use in the forward pass.
        :param D_in:
        :param H:
        :param D_out:
        """
        super(DynamicNet, self).__init__()
        self.input_linear = torch.nn.Linear(D_in, H)
        self.middle_Linear = torch.nn.Linear(H, H)
        self.output_linear = torch.nn.Linear(H, D_out)

    def forward(self, x):
        """
        For the forward pass of the model, we randomly choose either 0,1, 2 or 3 and reuse the middle_linear Module
        tha many time to compute hidden layer representations.

        Since each forwards pass builds a dynamic computation graph, we can use normal Python control-flow operators
        like loops or conditional statements when defining the forward pass of the model.

        Here we also see that it is perfectly safe to reuse the same Module many time when defining a computational
        graph. This is a big improvement from Lua Torch, where each Module could be used only once
        :param x:
        :return:
        """
        h_relu = self.input_linear(x).clamp(min=0)
        for _ in range(random.randint(0, 3)):
            h_relu = self.middle_Linear(h_relu).clamp(min=0)
        y_pred = self.output_linear(h_relu)
        return y_pred


dtype = torch.float
device = torch.device('gpu')

# N is batch size; D_in is input dimension
# H is hidden dimension, D_out is output dimension
N, D_in, H, D_out = 64, 1000, 100, 10

# Randomly initialize input and output data
x = torch.randn(N, D_in, device=device, dtype=dtype)
y = torch.randn(N, D_out, device=device, dtype=dtype)

# Randomly initialize weights for manual way
# w1 = torch.randn(D_in, H, device=device, dtype=dtype)
# w2 = torch.randn(H, D_out, device=device, dtype=dtype)

# Randomly initialize weights for auto_grad
w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True)
w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True)

learning_rate = 1e-6
for t in range(500):
    # Forward pass: compute predicted y the manual way
    # h = x.mm(w1)
    # h_relu = h.clamp(min=0)
    # y_pred = h_relu.mm(w2)

    # To apply our Function, we use Function.apply method. We alias this as relu
    relu = MyReLu.apply

    # Forward pass: compute predicted y the automatic way
    y_pred = x.mm(w1).clamp(min=0).mm(w2)

    # Compute and print loss
    loss = (y_pred - y).pow(2).sum().item()
    if t % 100 == 99:
        print(t, loss)

    # Backprop to compute gradients of w1 and w2 with respect to loss, the manual way
    # grad_y_prep = 2.0 * (y_pred - y)
    # grad_w2 = h_relu.t().mm(grad_y_prep)
    # grad_h_relu = grad_y_prep.mm(w2.t())
    # grad_h = grad_h_relu.clone()
    # grad_h[h < 0] = 0
    # grad_w1 = x.t().mm(grad_h)

    # Backprop to compute gradients the automatic way
    loss.backward()

    # Update weights using gradient descent, manually
    # w1 -= learning_rate * grad_w1
    # w2 -= learning_rate * grad_w2

    # Updating weights using gradient descent, automatic
    with torch.no_grad():
        w1 -= learning_rate * w1.grad
        w2 -= learning_rate * w2.grad

        # Manually zero the gradients after updating the weights
        w1.grad.zero_()
        w2.grad.zero_()


# implement the previous network with torch.nn
import torch.nn as nn
# N is batch size; D_in is input dimension
# H is hidden dimension, D_out is output dimension
N, D_in, H, D_out = 64, 1000, 100, 10

# create random Tensors to hold inputs and outputs
x = torch.randn(N, D_in)
y = torch.randn(N, D_out)

# Use the nn package to define our model as a sequence of layers. nn.Sequential
# is a Module which contains other Modules, and applies them in sequence to
# produce its output. Each Linear Module computes output from input using a
# linear function, and holds internal Tensors for its weight and bias.
model = nn.Sequential(
    nn.Linear(D_in, H),
    nn.ReLU(),
    nn.Linear(H, D_out)
)

# Construct our model by instantiating the class defined above
own_model = TwoLayerNet(D_in, H, D_out)

# Construct our model by instantiating the class defined above
dyn_model = DynamicNet(D_in, H, D_out)

# The nn package also contains definitions of popular loss functions; in this case we will use Mean Squared Error (MSE)
# as our loss function
loss_fn = nn.MSELoss(reduce='sum')
learning_rate = 1e-4
optimizer = torch.optim.Adam(model.paramters(), lr=learning_rate)
for t in range(500):
    # Forward pass: compute predicted y by passing x to the model. Module objects override the __call__ operator
    # so you can call them like functions. When doing so you pass a Tensor of input data to the Module and it produces
    # a Tensor of output data
    y_pred = model(x)

    # Compute and print loss. We pass Tensors containing the predicted and true values of y, and the loss function
    # returns a Tensor containing the loss.
    loss = loss_fn(y_pred, y)
    if t % 100 == 99:
        print(t, loss.item())

    # Zero the gradients before running the backpass
    # model.zero_grad()

    # Before the backward pass, use the optimizer object to zero all of the gradients for the variable it will update
    # (which are the learnable weights of the model). This is because by default, gradients are accumulated in buffers
    # (i.e. not overwritten) whenever .backward() is called. Checkout docs of torch.autograd.backward for more details.
    optimizer.zero_grad()

    # Backward pass: compute gradient of the loss with respect to all the learnable parameters of the model. Internally,
    # the parameters of each Module are stored in Tensors with requires_grad=True, so this call will compute gradients
    # for all learnable parameters in the model
    loss.backward()

    # Update the weights using gradient descent. Each parameter is a Tensor, so we can access its gradients like we did
    # before
    # with torch.no_grad():
    #    for param in model.parameters():
    #        param -= learning_rate * param.grad

    # Calling the step function on an Optimizer makes an update to its parameters
    optimizer.step()

