import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import random
import time


def kth_diag_indices(a, k):
    rows, cols = np.diag_indices_from(a)
    if k < 0:
        return rows[-k:], cols[:k]
    elif k > 0:
        return rows[:-k], cols[k:]
    else:
        return rows, cols


def mask_(matrices, maskval=0.0, mask_diagonal=True, is_random=False, future=False, future_ind=np.array([])):
    """
    Masks out all values in the given batch of matrices where i <= j holds,
    i < j if mask_diagonal is false
    In place operation
    :param tns:
    :return:
    """

    b, h, w = matrices.size()

    indices = torch.triu_indices(h, w, offset=0 if mask_diagonal else 1)
    ind = indices[1]
    last_pos = len(matrices[0][0]) - 1
    end_val = torch.tensor([[last_pos]], dtype=torch.int64)
    last_val = matrices[b-1, h-1, w-1]
    moving = True
    future_index_arr = []
    new_letter_ind = []
    if is_random:
        # for 20 steps select a random number between 0 and h-1, create a zero tensor to later point to the same
        # position and replace every indice with this position that is of value rand
        # these rand positions are now beeing ignored in the replacement with the maskval and therefore not masked.
        if moving:
            # moves the future words with the already predicted sequence, tp ensure a constant access to future words
            start, end, diff = 0, 10, 0
            future_index_arr = []
            for i in range(0, w-1):
                if 20 < diff < 26:
                    arr = np.random.randint(start, end, 5)
                elif 25 < diff < 30:
                    arr = np.random.randint(start, end, 2)
                elif diff >= 29:
                    arr = np.array([end])
                else:
                    if not future_ind.size == 0:
                        arr = future_ind + start
                    else:
                        arr = np.random.randint(start, end, 10)
                arr = np.array(arr)
                future_index_arr = np.concatenate((future_index_arr, arr), axis=0)
                arr = arr % w
                new_letter_ind.append(arr)
                diff += 1
                start += w - diff
                end += w - diff
                # end += w - 1 - diff  # if you want the end position to always be at the last position
                if end > len(ind) - 1:  # prevents the index to run out of bounds
                    end = len(ind) - 1
            ind[future_index_arr] = end_val
        else:
            # removes x many positions between 1 and y randomly from the masking procedure
            arr = np.array([], dtype=np.int)
            for _ in range(0, 5):
                rand = random.randint(1, 16)
                ind = torch.where(ind != rand, ind, end_val)
                arr = np.append(arr, rand % w)
            new_letter_ind = np.full((32, 5), arr)
    elif future:
        # removes a set amount of positions from the masking procedure, and keeping it constant over the trainings-time
        step_size = int(h / 20)
        for i in range(0, h, step_size):
            ind = torch.where(ind != i, ind, end_val)
            future_index_arr = np.concatenate((future_index_arr, i), axis=0)
    change_future_letters = True
    # Changes the letters that are being show as future letters, by choosing a random letter in the chosen surrounding
    # and places it at the future letters position
    if change_future_letters:
        max_val = torch.tensor(w-1, dtype=torch.long)
        min_val = torch.tensor(1, dtype=torch.long)
        new_letter_ind = np.array(new_letter_ind)
        change_ind_arr = new_letter_ind
        new_letter = []
        # moving = True
        if moving:
            for i in range(len(new_letter_ind)):
                # needs to be done in one step instead of for every row in new_letter_ind, as this it to slow for longer sequences
                vari = np.random.randint(-3, 3, len(new_letter_ind[i]))
                new_letter_ind[i] = new_letter_ind[i] + vari
                new_letter_ind[i] = np.where(new_letter_ind[i] > max_val.item(), max_val, new_letter_ind[i])
                new_letter_ind[i] = np.where(new_letter_ind[i] < min_val.item(), min_val, new_letter_ind[i])
                new_letter.append(matrices[:, 0, new_letter_ind[i]])
                matrices[:, i, change_ind_arr[i]] = new_letter[i]
        else:
            # faster hopefully
            vari = np.random.randint(-3, 3, new_letter_ind.shape)
            new_letter_ind = new_letter_ind + vari
            new_letter_ind[new_letter_ind >= max_val] = max_val
            new_letter_ind[new_letter_ind <= min_val] = min_val
            new_letter = matrices[:,]
    indices[1] = ind
    matrices[:, indices[0], indices[1]] = maskval
    matrices[b-1, h-1, w-1] = last_val


def d(tensor=None):
    """
    Returns a device string either for the best available device,
    or for the device corresponding to the argument
    :param tensor:
    :return:
    """
    if tensor is None:
        return 'cuda' if torch.cuda.is_available() else 'cpu'
    return 'cuda' if tensor.is_cuda else 'cpu'


class SelfAttentionWide(nn.Module):
    def __init__(self, emb, heads=8, mask=False, random=False, future=False, future_ind=np.array([])):
        """
        :param emb:
        :param heads:
        :param mask:
        """

        super().__init__()

        self.emb = emb
        self.heads = heads
        self.mask = mask
        self.random = random
        self.future = future
        self.future_ind = future_ind

        self.tokeys = nn.Linear(emb, emb * heads, bias=False)
        self.toqueries = nn.Linear(emb, emb * heads, bias=False)
        self.tovalues = nn.Linear(emb, emb * heads, bias=False)

        self.unifyheads = nn.Linear(heads * emb, emb)

    def forward(self, x):

        b, t, e = x.size()
        h = self.heads
        assert e == self.emb, f'Input embedding dim ({e}) should match layer embedding dim ({self.emb})'

        keys    = self.tokeys(x)   .view(b, t, h, e)
        queries = self.toqueries(x).view(b, t, h, e)
        values  = self.tovalues(x) .view(b, t, h, e)

        # compute scaled dot-product self-attention

        # - fold heads into the batch dimension
        keys = keys.transpose(1, 2).contiguous().view(b * h, t, e)
        queries = queries.transpose(1, 2).contiguous().view(b * h, t, e)
        values = values.transpose(1, 2).contiguous().view(b * h, t, e)

        queries = queries / (e ** (1/4))
        keys    = keys / (e ** (1/4))
        # - Instead of dividing the dot products by sqrt(e), we scale the keys and values.
        #   This should be more memory efficient

        # - get dot product of queries and keys, and scale
        dot = torch.bmm(queries, keys.transpose(1, 2))

        assert dot.size() == (b*h, t, t)

        if self.mask:  # mask out the upper half of the dot matrix, excluding the diagonal
            mask_(dot, maskval=float('-inf'), mask_diagonal=False, is_random=self.random, future=self.future, future_ind=self.future_ind)
        dot = F.softmax(dot, dim=2)
        # - dot now has row-wise self-attention probabilities

        # apply the self attention to the values
        out = torch.bmm(dot, values).view(b, h, t, e)

        # swap h, t back, unify heads
        out = out.transpose(1, 2).contiguous().view(b, t, h * e)
        return self.unifyheads(out)


class SelfAttentionNarrow(nn.Module):

    def __init__(self, emb, heads=8, mask=False, random=False, future=False):
        """
        :param emb:
        :param heads:
        :param mask:
        """

        super().__init__()

        assert emb % heads == 0, f'Embedding dimension ({emb}) should be divisible by nr. of heads ({heads})'

        self.emb = emb
        self.heads = heads
        self.mask = mask
        self.random = random
        self.future = future

        s = emb // heads
        # - We will break the embedding into `heads` chunks and feed each to a different attention head

        self.tokeys    = nn.Linear(s, s, bias=False)
        self.toqueries = nn.Linear(s, s, bias=False)
        self.tovalues  = nn.Linear(s, s, bias=False)

        self.unifyheads = nn.Linear(heads * s, emb)

    def forward(self, x):

        b, t, e = x.size()
        h = self.heads
        assert e == self.emb, f'Input embedding dim ({e}) should match layer embedding dim ({self.emb})'

        s = e // h
        x = x.view(b, t, h, s)

        keys    = self.tokeys(x)
        queries = self.toqueries(x)
        values  = self.tovalues(x)

        assert keys.size() == (b, t, h, s)
        assert queries.size() == (b, t, h, s)
        assert values.size() == (b, t, h, s)

        # Compute scaled dot-product self-attention

        # - fold heads into the batch dimension
        keys = keys.transpose(1, 2).contiguous().view(b * h, t, s)
        queries = queries.transpose(1, 2).contiguous().view(b * h, t, s)
        values = values.transpose(1, 2).contiguous().view(b * h, t, s)

        queries = queries / (e ** (1/4))
        keys    = keys / (e ** (1/4))
        # - Instead of dividing the dot products by sqrt(e), we scale the keys and values.
        #   This should be more memory efficient

        # - get dot product of queries and keys, and scale
        dot = torch.bmm(queries, keys.transpose(1, 2))

        assert dot.size() == (b*h, t, t)

        if self.mask: # mask out the upper half of the dot matrix, excluding the diagonal
            mask_(dot, maskval=float('-inf'), mask_diagonal=False, is_random=self.random, future=self.future)

        dot = F.softmax(dot, dim=2)
        # - dot now has row-wise self-attention probabilities

        # apply the self attention to the values
        out = torch.bmm(dot, values).view(b, h, t, s)

        # swap h, t back, unify heads
        out = out.transpose(1, 2).contiguous().view(b, t, s * h)

        return self.unifyheads(out)


class TransformerBlock(nn.Module):

    def __init__(self, emb, heads, mask, seq_length, ff_hidden_mult=4, dropout=0.0, wide=True, random=False,
                 future=False, future_ind=np.array([])):
        super().__init__()
        self.attention = SelfAttentionWide(emb, heads=heads, mask=mask, random=random, future=future, future_ind=future_ind) if wide \
            else SelfAttentionNarrow(emb, heads=heads, mask=mask, random=random, future=future)
        self.mask = mask
        self.norm1 = nn.LayerNorm(emb)
        self.norm2 = nn.LayerNorm(emb)

        self.ff = nn.Sequential(
            nn.Linear(emb, ff_hidden_mult * emb),
            nn.ReLU(),
            nn.Linear(ff_hidden_mult * emb, emb)
        )

        self.do = nn.Dropout(dropout)

    def forward(self, x):
        attended = self.attention(x)
        x = self.norm1(attended + x)
        # print(torch.mean(x))

        x = self.do(x)

        fedforward = self.ff(x)

        x = self.norm2(fedforward + x)

        x = self.do(x)

        return x


class GTransformer(nn.Module):
    """
    Transformer for generating text (character by character).
    """

    def __init__(self, emb, heads, depth, seq_length, num_tokens, wide=True, random=False, future=False, only_first=False):
        super().__init__()
        # sets the masking positions for every transformer-block at the same position but keeps it random across
        # the entire trainings-time resetting every epoch
        self.future_ind = np.random.randint(1, 16, 32)
        self.num_tokens = num_tokens
        self.token_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=num_tokens)
        self.pos_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=seq_length)

        tblocks = []
        for i in range(depth):
            if only_first and i == 0:
                tblocks.append(
                    TransformerBlock(emb=emb, heads=heads, seq_length=seq_length, mask=True, wide=wide, random=random, future=future, future_ind=self.future_ind))
            elif only_first:
                tblocks.append(
                    TransformerBlock(emb=emb, heads=heads, seq_length=seq_length, mask=True, wide=wide, random=False, future=False))
            else:
                tblocks.append(
                    TransformerBlock(emb=emb, heads=heads, seq_length=seq_length, mask=True, wide=wide, random=random, future=future, future_ind=self.future_ind))

        self.tblocks = nn.Sequential(*tblocks)

        self.toprobs = nn.Linear(emb, num_tokens)

    def forward(self, x):
        """
        :param x: A (batch, sequence length) integer tensor of token indices.
        :return: predicted log-probability vectors for each token based on the preceding tokens.
        """
        tokens = self.token_embedding(x)
        b, t, e = tokens.size()

        positions = self.pos_embedding(torch.arange(t, device=d()))[None, :, :].expand(b, t, e)
        x = tokens + positions

        x = self.tblocks(x)

        x = self.toprobs(x.view(b*t, e)).view(b, t, self.num_tokens)

        return F.log_softmax(x, dim=2)


class CTransformer(nn.Module):
    """
    Transformer for classifying sequences
    """

    def __init__(self, emb, heads, depth, seq_length, num_tokens, num_classes, max_pool=True, dropout=0.0, wide=False):
        """
        :param emb: Embedding dimension
        :param heads: nr. of attention heads
        :param depth: Number of transformer blocks
        :param seq_length: Expected maximum sequence length
        :param num_tokens: Number of tokens (usually words) in the vocabulary
        :param num_classes: Number of classes.
        :param max_pool: If true, use global max pooling in the last layer. If false, use global
                         average pooling.
        """
        super().__init__()

        self.num_tokens, self.max_pool = num_tokens, max_pool

        self.token_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=num_tokens)
        self.pos_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=seq_length)

        tblocks = []
        for i in range(depth):
            tblocks.append(
                TransformerBlock(emb=emb, heads=heads, seq_length=seq_length, mask=False, dropout=dropout, wide=wide))

        self.tblocks = nn.Sequential(*tblocks)

        self.toprobs = nn.Linear(emb, num_classes)

        self.do = nn.Dropout(dropout)

    def forward(self, x):
        """
        :param x: A batch by sequence length integer tensor of token indices.
        :return: predicted log-probability vectors for each token based on the preceding tokens.
        """
        tokens = self.token_embedding(x)
        b, t, e = tokens.size()

        positions = self.pos_embedding(torch.arange(t, device=d()))[None, :, :].expand(b, t, e)
        x = tokens + positions
        x = self.do(x)

        x = self.tblocks(x)

        x = x.max(dim=1)[0] if self.max_pool else x.mean(dim=1) # pool over the time dimension

        x = self.toprobs(x)

        return F.log_softmax(x, dim=1)