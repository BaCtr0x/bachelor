import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
import random
import sys


def dev(tensor=None):
    """
    Returns a device string either for the best available device,
    or for the device corresponding to the argument
    :param tensor:
    :return:
    """
    if tensor is None:
        return 'cuda' if torch.cuda.is_available() else 'cpu'
    return 'cuda' if tensor.is_cuda else 'cpu'


def default_hparams():
    """
    The default hyper parameters to define the models specifics

    n_vocabs = number of vocabularies
    n_ctx = the maximum length of the context
    n_embd = the length of the embedding
    n_head = the number of heads
    n_layers = the number of layers
    :return: the hyper parameters as a dictionary
    """
    hparams = {
        "n_vocab": 0,
        "n_ctx": 1024,
        "n_embd": 768,
        "n_head": 12,
        "n_layers": 12,
        "random": False,
        "future": False
    }
    return hparams


def mask_(matrices, maskval=0.0, mask_diagonal=True, random=False, future=False):
    """
        Masks out all values in the given batch of matrices where i <= j holds,
        i < j if mask_diagonal is false
        In place operation
        :param tns:
        :return:
        """

    b, h, w = matrices.size()

    indices = torch.triu_indices(h, w, offset=0 if mask_diagonal else 1)
    ind = indices[1]
    if random:
        # for 20 steps select a random number between 0 and h-1, create a zero tensor to later point to the same
        # position and replace every indice with this position that is of value rand
        # these rand positions are now beeing ignored in the replacement with the maskval and therefore not masked.
        for _ in range(0, 20):
            rand = random.randint(0, h - 1)
            zero = torch.zeros(1, 1, dtype=torch.int64)
            ind = torch.where(ind != rand, ind, zero)
    elif future:
        step_size = int(h / 20)
        for i in range(0, h, step_size):
            ind = torch.where(ind != i, ind, 0)
    indices[1] = ind
    matrices[:, indices[0], indices[1]] = maskval


class SelfAttention(nn.Module):
    """
    the self attention module for the transformer
    """
    def __init__(self, k, heads=8, mask=False, random=False, future=False):
        """
        The initialization function
        :param k: is the number of embeddings
        :param heads: is the number of heads
        """
        super(SelfAttention, self).__init__()
        self.k, self.heads = k, heads
        self.mask = mask
        self.random = random
        self.future = future

        # These compute the queries, key and value for all heads (as single concatenated vector)
        self.tokeys = nn.Linear(k, k * heads, bias=False)
        self.toqueries = nn.Linear(k, k * heads, bias=False)

        self.tovalues = nn.Linear(k, k * heads, bias=False)

        # this unifies the outputs of the different heads into a single k-vector
        self.unifyheads = nn.Linear(heads * k, k)

    def forward(self, x):
        """
        The forward function which is the heart of the attention module
        The output of each linear module has size (b, t, h*k) which is reshaped to (b, t, h, k) and give each head
        it's own dimension, with b = minibatch dimension, t = #vectors in sequence, k = dimension of each vector of t
        :param x: is the input
        :return: return the calculated output
        """
        b, t, k = x.size()
        h = self.heads

        queries = self.toqueries(x).view(b, t, h, k)
        keys = self.tokeys(x).view(b, t, h, k)
        values = self.tovalues(x).view(b, t, h, k)

        # fold heads into the batch dimension
        keys = keys.transpose(1, 2).contiguous().view(b * h, t, k)
        queries = queries.transpose(1, 2).contiguous().view(b * h, t, k)
        values = values.transpose(1, 2).contiguous().view(b * h, t, k)

        queries = queries / (k ** (1/4))
        keys = keys / (k ** (1/4))

        # get dor product of queries and key, and scale
        dot = torch.bmm(queries, keys.transpose(1, 2))

        # this is the old version
        # dot has size (b*h, t, t) containing raw weights
        # indices = torch.triu_indices(t, t, offset=1)
        # dot[:, indices[0], indices[1]] = float('-inf')

        # mask out the upper half of the dot matrix, excluding the diagonal,
        # leaving wholes to simuulate object detection input
        if self.mask:
            mask_(dot, maskval=float('-inf'), mask_diagonal=False, random=self.random, future=self.future)

        dot = F.softmax(dot, dim=2)
        # dot now contains row-wise normalized weights

        # apply the self attention to the values
        out = torch.bmm(dot, values).view(b, h, t, k)

        # swap h, t back, unify heads
        out = out.transpose(1, 2).contiguous().view(b, t, h * k)
        return self.unifyheads(out)


class TransformerBlock(nn.Module):
    """
    The transformer block to put everything together that is needed per block
    """
    def __init__(self, k, heads, dropout=0.0, random=False, future=False):
        """
        The initialization function
        k is the dimension of each vector in a sequence of t vectors
        :param k: the number of tokens
        :param heads: the number of heads
        :param dropout: the value for the dropout function, 0.0 is the default implying no dropout
        """
        super(TransformerBlock, self).__init__()

        # initializes the self attention layer
        self.attention = SelfAttention(k, heads=heads, mask=True, random=random, future=future)

        # initializes two normalisation layers
        self.norm1 = nn.LayerNorm(k)
        self.norm2 = nn.LayerNorm(k)

        # initializes the feed forward module
        self.ff = nn.Sequential(
            nn.Linear(k, 4 * k),
            nn.ReLU(),
            nn.Linear(4 * k, k)
        )

        self.do = nn.Dropout(dropout)

    def forward(self, x):
        """
        the forward function for the transformerblock operation
        :param x: the input for the block
        :return: returns the calculated output
        """
        attended = self.attention(x)
        x = self.norm1(attended + x)
        x = self.do(x)

        fed_forward = self.ff(x)
        x = self.norm2(fed_forward + x)
        x = self.do(x)
        return x


class Transformer(nn.Module):
    """
    the text generation transformer that consists of x transformer blocks with token and position embedding
    """
    def __init__(self, hparams):
        """
        The initialization function
        :param hparams: the hyper parameters for the transformer
        """
        super(Transformer, self).__init__()
        # get the hyper parameters from the hparams dictionary
        self.num_tokens = hparams["n_vocab"]
        self.heads = hparams["n_head"]
        self.layers = hparams["n_layers"]
        self.context_length = hparams["n_ctx"]
        self.emb = hparams["n_embd"]
        self.random = hparams["random"]
        self.future = hparams["future"]

        # create token and positional embedding
        self.token_emb = nn.Embedding(embedding_dim=self.emb, num_embeddings=self.num_tokens)
        self.pos_emb = nn.Embedding(embedding_dim=self.emb, num_embeddings=self.context_length)

        # The sequence of transformer blocks that does all the heavy lifting
        # an array to store the transformer blocks
        tblocks = []
        for i in range(self.layers):
            tblocks.append(TransformerBlock(k=self.num_tokens, heads=self.heads, random=self.random, future=self.future))
        self.tblocks = nn.Sequential(*tblocks)

        # mapping the output to probability logits
        self.toprobs = nn.Linear(self.emb, self.num_tokens)

    def forward(self, x):
        """
        The forward module of the transformer
        :param x: A (batch, sequence length) integer tensor of token indices
        :return: predicted log-probability vector for each token based on the preceding tokens
        """
        tokens = self.token_emb(x)
        b, t, e = tokens.size()

        position = self.pos_emb(torch.arange(t, device=dev()))[None, :, :].expand(b, t, e)
        x = tokens + position

        x = self.tblocks(x)

        self.toprobs(x.view(b * t, e)).view(b, t, self.num_tokens)

        return F.log_softmax(x, dim=2)
